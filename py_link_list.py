#!/usr/bin/python3

# Node object to hold the data

class Node:
    def __init__(self, input_value=0):
        self.the_value = input_value
        self.next_node = None

# Linked list object to keep track of the nodes

class My_LinkedList:
    # construction
    def __init__(self, input_Node=None):
        self.head = input_Node

    # insert node objects to the system
    def insert(self, input_node):
        # insert into empty chain
        if self.head == None:
            self.head = input_node
        # if not empty, then new node will go to the end of the list, duh!
        else:
            temp_node = self.head
            # will not rest until it finds the last node in the chain
            while(temp_node.next_node != None):
                temp_node = temp_node.next_node
            # once found, we will assign the next node of this place holder
            # node(which is equal to the end node) to our input node
            temp_node.next_node = input_node
    
    # printing the members of the gang
    def display(self):
        temp_node = self.head
        while(temp_node != None):
            print(str(temp_node.the_value) + "==>", end="")
            temp_node = temp_node.next_node
        print("None")

    # removing the node
    def delete(self, in_val):

        # cannot remove from an empty thing!!
        if self.head == None:
            return -1
        
        # if head is decapitated, then, next node will step into the throne
        elif (self.head.the_value == in_val):
            self.head = self.head.next_node
        
        else:
            # base node will stop at a node before our intended node
            base_node = self.head
            # forward, will be one node ahead, together with base,
            # surronding the to be deleted node
            forward_node = self.head

            # keep going until you find the culprit
            while (forward_node.the_value != in_val):
                base_node = forward_node
                forward_node = forward_node.next_node
                if (forward_node == None):
                    return -1
            # jump one node ahead
            forward_node = forward_node.next_node
            # bypass the culprit node, and refer to the node after
            # base node to be forward node, garbage collector will remove the 
            # the stupid node
            base_node.next_node = forward_node

# the driver part of the program, fasten the seat belts
if __name__ == "__main__":
    zero = Node(1)
    a_list = My_LinkedList(zero)
    a_list.insert(Node(2))
    a_list.insert(Node(3))
    a_list.insert(Node(4))
    a_list.insert(Node(5))
    a_list.insert(Node(6))
    a_list.insert(Node(7))
    a_list.insert(Node(8))

    a_list.display()

    a_list.delete(4)

    a_list.display()

